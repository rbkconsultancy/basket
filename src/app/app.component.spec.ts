import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {MatChip, MatChipList, MatDivider, MatFormField, MatLabel, MatList, MatListItem} from '@angular/material';
import {ItemGroupsComponent} from './components/item-groups/item-groups.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatListModule, MatDividerModule,
  MatChipsModule, MatIconModule} from '@angular/material';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatCardModule
      ],
      declarations: [
        AppComponent, ItemGroupsComponent, MatChip, MatDivider, MatFormField, MatChipList, MatListItem, MatList, MatLabel
      ],
      providers: [MatChipsModule, MatDividerModule, MatCardModule, MatFormFieldModule, MatInputModule,
        MatListModule, MatDividerModule, MatIconModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
