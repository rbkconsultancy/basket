import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ItemGroupsComponent } from './components/item-groups/item-groups.component';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatListModule, MatDividerModule,
          MatChipsModule, MatIconModule} from '@angular/material';
import { SecondComponentComponent } from './components/second-component/second-component.component';
import { FirstComponentComponent } from './components/first-component/first-component.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemGroupsComponent,
    SecondComponentComponent,
    FirstComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule, MatFormFieldModule, MatInputModule, MatListModule, MatDividerModule,
    MatChipsModule, MatIconModule, MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
