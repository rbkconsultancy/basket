import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first-component',
  templateUrl: './first-component.component.html',
  styleUrls: ['./first-component.component.less']
})
export class FirstComponentComponent implements OnInit {
  // The data needs to be retrieved from an endpoint ideally.
  data = {
    'items': {
      'pens': 100,
      'sharpeners': 32,
      'pencils': 76,
      'fish': 12,
      'chicken': 13,
      'veg': 43,
      'chocolate': 4
    },
    'groups': {
      'stationery': ['pens', 'sharpeners', 'pencils'],
      'food': ['fish', 'chicken', 'veg', 'chocolate']
    }
  };

  newGroup: string;
  newItems: Array<string>;

  constructor() {
    this.newGroup = 'drinks';
    this.newItems = ['coke', 'pepsi', 'cola'];
  }

  ngOnInit() {
  }

}
