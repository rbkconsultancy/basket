import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-component',
  templateUrl: './second-component.component.html',
  styleUrls: ['./second-component.component.less']
})
export class SecondComponentComponent implements OnInit {
  // The data needs to be retrieved from an endpoint ideally.
  data = {
    'items': {
      'fiat': 10,
      'bentley': 2,
      'Mitsubishi': 6,
      'tomato': 15,
      'carrot': 13,
      'beans': 4,
      'spinach': 9
    },
    'groups': {
      'vehicles': ['fiat', 'bentley', 'Mitsubishi'],
      'vegetables': ['tomato', 'carrot', 'beans', 'spinach']
    }
  };
  newGroup: string;
  newItems: Array<string>;

  constructor() {
    this.newGroup = 'bike';
    this.newItems = ['yamaha', 'kawasaki', 'triumph'];
  }


  ngOnInit() {
  }

}
