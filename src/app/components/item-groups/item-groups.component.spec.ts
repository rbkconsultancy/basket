import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemGroupsComponent } from './item-groups.component';
import {ItemsGroupsService} from '../../services/items-groups.service';
import { MatCardModule, MatFormFieldModule, MatInputModule, MatListModule, MatDividerModule,
  MatChipsModule, MatIconModule} from '@angular/material';
import {MockGroupItemsData} from './test_data/mock-items-data';


describe('ItemGroupsComponent', () => {
  let component: ItemGroupsComponent;
  let fixture: ComponentFixture<ItemGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemGroupsComponent ],
      imports: [ MatCardModule, MatFormFieldModule, MatInputModule, MatListModule, MatDividerModule, MatChipsModule, MatIconModule],
      providers: [ ItemsGroupsService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.data = MockGroupItemsData.groupData;

  });

  it('[1] should create', () => {
    expect(component).toBeTruthy();
  });

  it('[2] should update first element to given number', () => {
    component.updateItems('food', 989);
    expect(component.data.items['fish']).toEqual(989);
    expect(component.data.items['chicken']).toEqual(0);
    expect(component.data.items['veg']).toEqual(0);
    expect(component.data.items['chocolate']).toEqual(0);
  });

  it('[3] should add new group drinks and its respective items', () => {
    component.newGroup = 'drinks';
    component.newItems = ['coke', 'pepsi', 'cola'];
    component.addStaticGroupItems();
    expect(component.data.groups['drinks']).toBeDefined();
  });

  it('[4] should remove one item chicken from group food', () => {
    component.removeGroupItem('food', 'chicken');
    expect(component.data.items['chicken']).toBeUndefined();
  });

  it('[5] should remove group food and respective items from the basket list', () => {
    component.removeGroupAndItems('food');
    expect(component.data.groups['food']).toBeUndefined();
  });
  it('[6] should remove stationery group when removing all respective items from the basket list', () => {
    component.removeGroupItem('stationery', 'pens');
    component.removeGroupItem('stationery', 'sharpeners');
    component.removeGroupItem('stationery', 'pencils');
    expect(component.data.groups['stationery']).toBeUndefined();
  });
});
