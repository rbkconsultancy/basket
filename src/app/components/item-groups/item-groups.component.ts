import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {ItemsGroupsService} from '../../services/items-groups.service';
import {ItemGroupsInterface} from '../../interfaces/item-groups-interface';

@Component({
  selector: 'app-item-groups',
  templateUrl: './item-groups.component.html',
  styleUrls: ['./item-groups.component.less']
})
export class ItemGroupsComponent implements OnInit {

  @Input() data: ItemGroupsInterface;
  @Input() newGroup: string;
  @Input() newItems: Array<string>;
  errorMessage: string;

  constructor(private itemGroupsService: ItemsGroupsService) {
    this.data = {'items': {}, 'groups': {}};
  }

  ngOnInit() {}

  updateItems = (key, groupValue) => {
    groupValue = parseInt(groupValue, 10);
    if (!groupValue || groupValue <= 0 || groupValue > 1000) {
      this.errorMessage = 'Item quantity must be between 0 and 1000';
    } else if (this.data) {
      this.errorMessage = '';
      this.data = this.itemGroupsService.updateSelectedGroupItemValue(key, this.data, groupValue);
    }
  }

  addStaticGroupItems = () => {
    this.errorMessage = '';
      this.data = this.itemGroupsService.addGroupdAndItems(this.data, this.newGroup, this.newItems);
  }

  removeGroupAndItems = (key) => {
      this.data = this.itemGroupsService.removeGroup(key, this.data);
      if (Object.entries(this.data.groups).length === 0 && this.data.groups.constructor === Object) {
        this.errorMessage = 'No group item data available';
      } else {
        this.errorMessage = '';
      }
  }

  removeGroupItem = (key, item) => {
      this.data = this.itemGroupsService.removeItemFromGroup(key, this.data, item);
  }
}
