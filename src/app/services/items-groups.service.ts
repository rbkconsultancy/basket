import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ItemsGroupsService {

  constructor() { }


  updateSelectedGroupItemValue = (key, data, groupValue) => {
    // Update the user input value to the first item of the group item and update all other items in respective group to 0.
    const firstItemToChange = data['groups'][key][0];

    this.arrayContainsArray(data['groups'][key], data['items']);
    data['items'][firstItemToChange] = groupValue;

    return data;
  }

  arrayContainsArray (superset, subset) {
    superset.forEach(function(val, index) {
      subset[val] = 0;
    });
  }


  addGroupdAndItems = (data, newGroup: string, newItems: Array<string> = []) => {
    // Ideally we can pull this data through an endpoint or from a dataset json in a random order.  After pulling data delete
    // the group from the data source.

    if (!!newGroup && newItems.length > 0) {
      data['groups'][newGroup] = newItems;

      newItems.forEach(function (val, index) {
        data['items'][val] = 0;
      });
      return data;
    }
  }

  removeGroup = (key, data) => {
    // Remove whole group.
    data = this.removeAllItemsFromGroup(key, data);
    delete data['groups'][key];
    return data;
  }

  removeAllItemsFromGroup = (key, data) => {
    const itemsToRemove = data['groups'][key];
    const items = data.items;

    itemsToRemove.forEach(function(val, index) {
      delete data['items'][val];
    });
    return data;
  }

  removeItemFromGroup = (key, data, item) => {
    // Remove a single item from a given group.  If all items have been removed then the group also will be removed.
    const group = data['groups'][key];
    const idx = group.indexOf(item);
    if (idx > -1) {
      group.splice(idx, 1);
    }

    if (group.length === 0) {
      this.removeGroup(key, data);
    }


    const items = data['items'];
    const newItems = Object.keys(items).reduce((object, itemKey) => {
      if (itemKey !== item) {
        object[itemKey] = items[itemKey];
      }
      return object;
    }, {});

    data['items'] = newItems;
    return data;

  }

}

