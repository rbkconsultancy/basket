import { TestBed } from '@angular/core/testing';

import { ItemsGroupsService } from './items-groups.service';

describe('ItemsGroupsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemsGroupsService = TestBed.get(ItemsGroupsService);
    expect(service).toBeTruthy();
  });
});
